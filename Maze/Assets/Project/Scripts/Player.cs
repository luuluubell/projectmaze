﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class Player : MonoBehaviour
{
    Vector3 _jump; 
    [SerializeField] float jumpPower = 30f; 
    bool grounded; 
    Rigidbody rb; 
    RigidbodyConstraints originalConstraints; 
    GameAction playerJump; 

    [SerializeField] GameObject HtpScreen; 
    [SerializeField] GameObject ReplayScreen1; 
    [SerializeField] GameObject ReplayScreen2; 

    void Awake () 
    {
        playerJump = new GameAction(); 
        playerJump.Enable(); 
        playerJump.Actions.Jump.performed += Jump; 
    }

    void Start ()
    {
        rb = GetComponent<Rigidbody>(); 
        _jump = new Vector3 (0, 2, 0); 
        originalConstraints = rb.constraints; 
        StartCoroutine(FreezeBall());
    }

    void OnDestroy ()
    {
        playerJump.Actions.Jump.performed -= Jump;
    }

    void OnCollisionStay(Collision other) {
        grounded = true; 
    }

    void OnCollisionExit ()
    {
        grounded = false; 
    }

    void Jump(InputAction.CallbackContext ctx)
    {
        if (grounded)
        {
            rb.AddForce (_jump * jumpPower, ForceMode.Impulse); 
            grounded = false; 
        }
    }

    IEnumerator FreezeBall ()
    {
        rb.constraints = RigidbodyConstraints.FreezePositionY; 
        yield return new WaitForSeconds(5f); 

        rb.constraints = originalConstraints; 
        HtpScreen.SetActive(false);
    }

    private void OnTriggerEnter(Collider other) 
    {
        if (other.tag == "Lava")
        {
            Time.timeScale = 0f; 
            ReplayScreen2.SetActive(true);
        }  

        if (other.tag == "Plant")
        {
            Time.timeScale = 0f; 
            ReplayScreen1.SetActive(true);  
        }   
    }
}
