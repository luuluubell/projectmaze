﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class PauseMenu : MonoBehaviour
{
    public GameObject PauseCanvas;
    public GameObject PauseButton;  

    public void PauseGame ()
    {   
        //Debug.Log("paused");
        PauseButton.SetActive(false); 
        Time.timeScale = 0f; 
        PauseCanvas.SetActive(true); 
    }

    public void ResumeGame ()
    {
        //Debug.Log("resumed");
        Time.timeScale = 1f;
        PauseButton.SetActive(true); 
        PauseCanvas.SetActive(false); 
    }

    public void MainMenu ()
    {
        SceneManager.LoadScene ("MainMenu"); 
    }

    public void QuitGame ()
    {
        //Debug.Log("I leave now");
        Application.Quit(); 
    }

}
