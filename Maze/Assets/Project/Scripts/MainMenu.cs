﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI; 
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{
    public void PlayGame ()
    {
        //Debug.Log("I load");
        SceneManager.LoadScene ("GameScene");
    }   

    public void QuitApp ()
    {
        //Debug.Log("I leave now");
        Application.Quit(); 
    }
}
