﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Block : MonoBehaviour
{
    [SerializeField] GameObject endPoint; 
    bool moving; 
    float speed = 0.5f; 
    Vector3 dir; //find a way to lock the other axis' 

    private void Awake() {
        moving = true; 
        StartCoroutine(RemoveBlocks()); 
    }

    void FixedUpdate() //x axis tilt
    {
        dir.x = Input.acceleration.x; 
        transform.Translate(Vector3.Normalize(endPoint.transform.position - transform.position) * speed);  

        if (dir.x >= 0)
        {
            speed = 0.5f *2; 
        }

        if (dir.x == 0)
        {
            speed = 0.5f; 
        }

        if(dir.x <= 0)
        {
            speed = 0.5f/2; 
        }
    }

    IEnumerator RemoveBlocks ()
    {
        while (moving)
        {
            yield return null; 

            if (transform.position.x <= -149)
            {
                GameObject.DestroyImmediate(transform.gameObject, true); 
            }
        }

    }

}
