﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndlessWorldSpawner : MonoBehaviour
{
    [SerializeField] GameObject floor; 
     GameObject currentFloor; 
    //[SerializeField] GameObject endPoint; 
    //GameObject floorClone; 
    
    private void Start()
    {
        Time.timeScale = 1f; 
        StartCoroutine(SpawnFloor()); 
    }

    IEnumerator SpawnFloor()
    {
        
        while (true)
        {
        currentFloor = Instantiate(floor, transform.position, Quaternion.identity);

        yield return new WaitForSeconds (Random.Range(3, 10)); 

        }
    }

}