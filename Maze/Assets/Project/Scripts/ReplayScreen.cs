﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement; 
using UnityEngine.UI; 

public class ReplayScreen : MonoBehaviour
{
    public void ReplayGame ()
    {
        SceneManager.LoadScene ("GameScene");
    }

    public void MainMenu ()
    {
        SceneManager.LoadScene ("MainMenu");
    }

    public void Form ()
    {
        SceneManager.LoadScene ("Form"); 
    }
}
