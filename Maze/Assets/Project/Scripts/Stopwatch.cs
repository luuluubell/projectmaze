﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI; 

public class Stopwatch : MonoBehaviour
{
    public Text timer;
    float time; 
    float msec; 
    float sec;
    float min; 

    public static bool gamePaused = false; 

    public void StopStopwatch()
    {
        StopCoroutine("StopWatch"); 
    }
    
    public void ResumeStopwatch ()
    {
        StartCoroutine("StopWatch"); 
    }

    private void Start()
    {
        StartCoroutine("StopWatch"); 
        gamePaused = false; 
    }

    public IEnumerator StopWatch()
    {
        while (gamePaused == false)
        {
            time += Time.deltaTime; 
            msec = (int)((time - (int)time) * 100);
            sec = (int)(time % 60); 
            min = (int)(time / 60 % 60);

            timer.text = string.Format("{0:00}:{1:00}:{2:00}", min,sec,msec);

            yield return null; 
        }
    }
}

//reference https://youtu.be/K2aFVNNd7yQ